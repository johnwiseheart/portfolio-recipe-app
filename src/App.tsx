import React from 'react'
import { Root, Routes } from 'react-static'
import './app.css'
import { style } from 'typestyle'
import ReactGA from 'react-ga'
import {
  Router,
  LocationProvider,
  createHistory,
  createMemorySource,
} from '@reach/router'
import { PulseLoader } from 'react-spinners'

ReactGA.initialize('UA-139517135-1')

let history =
  typeof window !== 'undefined'
    ? createHistory(window as any)
    : createHistory(createMemorySource('/'))

class App extends React.PureComponent {
  componentDidMount() {
    if (typeof window !== 'undefined') {
      ReactGA.pageview(window.location.pathname)
    }
    history.listen(({ location }) => {
      ReactGA.pageview(location.pathname)
    })
  }

  render() {
    return (
      <Root>
        <React.Suspense
          fallback={
            <div className={Styles.loader}>
              <PulseLoader color="#999" />
            </div>
          }
        >
          <LocationProvider history={history}>
            <Router>
              <Routes path="*" />
            </Router>
          </LocationProvider>
        </React.Suspense>
      </Root>
    )
  }
}

export default App

const Styles = {
  loader: style({
    margin: '0 auto',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
  }),
}
