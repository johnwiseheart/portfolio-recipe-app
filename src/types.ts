export interface GitlabProject {
  created_at: string
  description: string
  last_activity_at: string
  name: string
  tag_list: string[]
  web_url: string
}

export interface Project {
  created_at: string
  description: string
  maintained: boolean
  name: string
  repo_url: string
  updated_at: string
  url: string | undefined
}

export interface Recipe {
  data: {
    date: string
    instagram_hashes: string[]
  }
  content: string
  name: string
  slug: string
}
