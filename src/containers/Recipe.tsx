import React from 'react'
import { Head, useRouteData } from 'react-static'
import { Link } from '@reach/router'
import { Recipe } from '../types'
import * as ReactMarkdown from 'react-markdown'
import { style, media } from 'typestyle'
import { PageWrapper } from 'components/PageWrapper'
import { InstagramThumbnail } from 'components/InstagramThumbnail'

export default () => {
  const { recipe }: { recipe: Recipe } = useRouteData()
  return (
    <PageWrapper header={<Link to="/recipes/">{'<'} Back to recipes</Link>}>
      <Head title={`${recipe.name} : Recipes : John Wiseheart`} />
      <InstagramThumbnail
        className={Styles.images}
        hashes={recipe.data.instagram_hashes}
      />
      <ReactMarkdown className={Styles.markdown} source={recipe.content} />
    </PageWrapper>
  )
}

const Styles = {
  markdown: style({
    $nest: {
      h1: {
        textTransform: 'uppercase',
      },
      h2: {
        textTransform: 'uppercase',
      },
      strong: {
        textTransform: 'uppercase',
      },
      ul: {
        paddingLeft: 30,
        $nest: {
          li: {
            paddingLeft: 10,
          },
        },
      },
    },
  }),
  link: style({
    padding: 10,
  }),
  images: style(
    {
      marginTop: 20,
      maxWidth: 150,
    },
    media({ minWidth: 600 }, { float: 'right' }),
  ),
}
