import React from 'react'
import { Header } from 'components/Header'
import { Section } from 'components/Section'
import { PageWrapper } from 'components/PageWrapper'

export default () => (
  <PageWrapper>
    <Header>Page not found.</Header>
    <Section content="What a pity!" />
  </PageWrapper>
)
