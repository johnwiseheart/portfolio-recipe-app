import React from 'react'
import { Head, useRouteData } from 'react-static'
import { Recipe } from '../types'
import { Header } from 'components/Header'
import { RecipeTile } from 'components/RecipeTile'
import { style, media } from 'typestyle'
import { Section } from 'components/Section'
import { PageWrapper } from 'components/PageWrapper'
import { Link } from '@reach/router'

export default () => {
  const { recipes }: { recipes: Recipe[] } = useRouteData()
  const content = `Hello! I like to bake and cook.
  I got sick of losing track of recipes that I liked, and having to scroll through pages of
  text to get to actual recipes on the blogs you find online. This website serves as a record
  of the recipes I've used in the past, with updates as I make improvements to them.`

  return (
    <PageWrapper header={<Link to="/">{'<'} Back to home</Link>}>
      <Head title="Recipes : John Wiseheart" />
      <Header>John's Recipes</Header>

      <Section content={content} />

      <ul className={Styles.list}>
        {recipes
          .sort((recipeA, recipeB) =>
            recipeB.data.date.localeCompare(recipeA.data.date),
          )
          .map(recipe => (
            <li className={Styles.listItem} key={recipe.slug}>
              <RecipeTile recipe={recipe} />
            </li>
          ))}
      </ul>
    </PageWrapper>
  )
}

const Styles = {
  list: style({
    listStyle: 'none',
    padding: 0,
    margin: 0,
    display: 'flex',
    flexWrap: 'wrap',
  }),
  listItem: style(
    {
      width: '49%',
      marginTop: '2%',
      $nest: {
        '&:nth-child(odd)': {
          marginRight: '2%',
        },
        '&:before': {
          content: `""`,
          float: 'left',
          paddingTop: '100%',
        },
      },
    },
    media(
      { minWidth: 600 },
      {
        width: '23.5%',
        $nest: {
          '&:not(:nth-child(4n))': {
            marginRight: '2%',
          },
        },
      },
    ),
  ),
  tile: style({
    height: '100%',
  }),
}
