import React, { useState, useEffect } from 'react'
import { Head } from 'react-static'
import { getProjects } from '../utils/projects'
import { Project } from '../types'
import { Section } from '../components/Section'
import { Header } from '../components/Header'
import { PageWrapper } from '../components/PageWrapper'
import { Link } from '@reach/router'

export default () => {
  const [hasStartedLoading, setHasStartedLoading] = useState(false)
  const [projects, setProjects] = useState<Project[]>([])

  useEffect(() => {
    if (!hasStartedLoading) {
      setHasStartedLoading(true)
      getProjects().then(setProjects)
    }
  })

  return (
    <PageWrapper rightHeader={<Link to="/recipes">Recipes {'>'}</Link>}>
      <Head title="John Wiseheart" />
      <Header>John Wiseheart</Header>
      <Section
        content="Hi there! I'm a full-stack software engineer from Sydney currently
        living in Seattle, Washington and working at Palantir. I studied Computer Science
        at UNSW and graduated in 2016. I prefer working with Typescript and
        React, but also have experience with Javascript, Python and Java."
      />
      <Section
        content={
          <>
            You can find a history of my personal projects on{' '}
            <a href="https://gitlab.com/johnwiseheart">GitLab</a>.
          </>
        }
      />
      <Section
        content={
          <>
            I like to bake! I keep a list of the recipes that I've made on my{' '}
            <a href="https://jcaw.me/recipes">recipes site</a>, and regularly
            update them as I make improvements.
          </>
        }
      />
      <Section title="Projects" content={projects.filter(p => p.maintained)} />
      <Section title="Archive" content={projects.filter(p => !p.maintained)} />
      <Section
        title="Contact"
        content="You can contact me by email at johnwiseheart@gmail.com."
      />
    </PageWrapper>
  )
}
