import { Project, GitlabProject } from '../types'

export const getProjects = async (): Promise<Project[]> => {
  const projects = await getGitlabProjects()
  const otherProjects = await getOtherProjects()

  return [...projects, ...otherProjects].sort((projectA, projectB) =>
    projectA.name.localeCompare(projectB.name),
  )
}

const getGitlabProjects = async (): Promise<Project[]> => {
  const projects: GitlabProject[] = await fetch(
    'https://gitlab.com/api/v4/users/johnwiseheart/projects',
  ).then(resp => resp.json())

  return projects
    .filter(
      project =>
        project.tag_list.indexOf('website') > -1 ||
        project.tag_list.indexOf('website-demo') > -1,
    )
    .map(project => {
      const {
        created_at,
        description,
        last_activity_at,
        name,
        tag_list,
        web_url,
      } = project

      const urlTag = tag_list.find(tag => tag.startsWith('url='))
      const url = urlTag !== undefined ? urlTag.split('url=')[1] : undefined

      return {
        created_at,
        description,
        maintained: tag_list.indexOf('website-demo') > -1,
        name,
        repo_url: web_url,
        updated_at: last_activity_at,
        url,
      }
    })
}

const getOtherProjects = (): Promise<Project[]> => {
  return new Promise(resolve => resolve(require('./otherProjects.json')))
}
