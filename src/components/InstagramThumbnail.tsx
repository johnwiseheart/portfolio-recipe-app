import React, { useState } from 'react'
import { style, keyframes, classes } from 'typestyle'

export const InstagramThumbnail: React.FC<{
  className?: string
  hashes: string[] | undefined
  firstOnly?: boolean
}> = ({ className, hashes, firstOnly }) => {
  const [imageLoaded, setImageLoaded] = useState(false)
  const [loaderWidth] = useState(100)

  if (hashes === undefined) {
    return null
  }

  const loader = !imageLoaded ? (
    <div className={Styles.loader(loaderWidth)} />
  ) : (
    undefined
  )

  const hashesToRender = firstOnly ? [hashes[0]] : hashes

  const thumbnails = hashesToRender.map(hash => (
    <img
      className={Styles.image}
      key={hash}
      src={`https://instagram.com/p/${hash}/media/`}
      onLoad={() => setImageLoaded(true)}
    />
  ))
  return (
    <div className={classes(className, Styles.container)}>
      {loader}
      {thumbnails}
    </div>
  )
}

const colorAnimationName = (width: number) =>
  keyframes({
    '0%': {
      backgroundPosition: 0,
    },
    '100%': {
      backgroundPosition: width,
    },
  })

const Styles = {
  image: style({
    display: 'block',
    backgroundColor: '#eee',
  }),
  container: style({
    height: '100%',
    backgroundColor: '#ededed',
    alignItems: 'center',
    display: 'flex',
    $nest: {
      '> img:nth-child(odd)': {
        marginBottom: 10,
      },
      '> img:only-child': {
        marginBottom: 0,
      },
    },
  }),
  loader: (width: number) =>
    style({
      height: '100%',
      animationName: colorAnimationName(width),
      animationDuration: '1s',
      animationIterationCount: 'infinite',
      animationTimingFunction: 'linear',
      backgroundImage: 'linear-gradient(90deg, #eee, #e8e8e8, #eee)',
    }),
}
