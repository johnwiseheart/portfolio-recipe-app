import React from 'react'
import { style, media } from 'typestyle'

interface PageWrapperProps {
  header?: JSX.Element
  rightHeader?: JSX.Element
}

export const PageWrapper: React.SFC<PageWrapperProps> = ({
  children,
  header,
  rightHeader,
}) => {
  return (
    <>
      <div className={Styles.header}>
        {header !== undefined ? <div>{header}</div> : undefined}
        {rightHeader !== undefined ? (
          <div className={Styles.right}>{rightHeader}</div>
        ) : (
          undefined
        )}
      </div>
      <div className={Styles.app}>{children}</div>
    </>
  )
}

const Styles = {
  app: style(
    {
      maxWidth: 640,
      margin: '0 auto',
      padding: '60px 20px 20px 20px',
    },
    media(
      { maxWidth: 600 },
      {
        padding: 20,
      },
    ),
  ),
  header: style({
    margin: 10,
  }),
  right: style({
    textAlign: 'right',
  }),
}
