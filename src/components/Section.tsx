import React from 'react'
import { style } from 'typestyle'
import { Project } from '../types'

const isArray = (x: string | JSX.Element | Project[]): x is Project[] => {
  return x instanceof Array
}

export interface SectionProps {
  title?: string
  content: string | JSX.Element | Project[]
  loading?: boolean
}

export const Section: React.SFC<SectionProps> = ({
  title,
  content,
  loading,
}: SectionProps) => {
  const header =
    title !== undefined ? (
      <header className={Styles.header}>{title}</header>
    ) : (
      undefined
    )

  const body = isArray(content) ? (
    <ul className={Styles.list}>
      {content.map(({ name, description, url, repo_url }) => (
        <li key={name} className={Styles.listItem}>
          <div className={Styles.listItemTitle}>
            <a href={url || repo_url}>{name}</a>
          </div>
          <div className={Styles.listItemDivider}>:</div>
          <div>{description}</div>
        </li>
      ))}
    </ul>
  ) : (
    <p className={Styles.text}>{content}</p>
  )

  const loadingIndicator = loading ? 'Loading...' : undefined

  return (
    <section className={Styles.container}>
      {header}
      <div className={Styles.body}>
        {loadingIndicator}
        {body}
      </div>
    </section>
  )
}

const Styles = {
  body: style({
    color: '#424242',
  }),
  container: style({
    marginBottom: 20,
  }),
  header: style({
    textTransform: 'uppercase',
    fontWeight: 'bolder',
  }),
  list: style({
    listStyle: 'none',
    padding: 0,
  }),
  listItem: style({
    display: 'flex',
  }),
  listItemDivider: style({
    margin: '0 5px',
  }),
  listItemTitle: style({
    minWidth: 100,
  }),
  text: style({
    textAlign: 'justify',
  }),
}
