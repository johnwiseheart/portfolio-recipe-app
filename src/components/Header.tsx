import React from 'react'
import { style } from 'typestyle'

export const Header: React.FC = ({ children }) => {
  return <header className={Styles.header}>{children}</header>
}

const Styles = {
  header: style({
    textTransform: 'uppercase',
    fontWeight: 'bold',
    fontSize: '2em',
  }),
}
