import React from 'react'
import { Recipe } from '../types'
import { InstagramThumbnail } from './InstagramThumbnail'
import { Link } from '@reach/router'
import { style } from 'typestyle'

export const RecipeTile: React.SFC<{
  recipe: Recipe
}> = ({ recipe }) => {
  const tileContent =
    recipe.data.instagram_hashes !== undefined ? (
      <InstagramThumbnail
        hashes={recipe.data.instagram_hashes}
        firstOnly={true}
      />
    ) : (
      <div className={Styles.noThumb}>{recipe.name}</div>
    )

  return (
    <Link className={Styles.link} to={`/recipes/${recipe.slug}/`}>
      {tileContent}
    </Link>
  )
}

const Styles = {
  noThumb: style({
    border: '2px solid gray',
    height: 'calc(100% - 4px)',
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }),
  link: style({
    height: '100%',
  }),
}
