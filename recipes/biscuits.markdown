---
date: 2018-04-22 16:30:00 -0700
instagram_hashes:
  - BgxT9haFHCJ
---

# Biscuits

## Ingredients

- 3 cups all-purpose flour
- 3 Tbls sugar
- 1/2 tsp salt
- 4 tsp baking powder
- 1/2 tsp cream of tartar
- 3/4 cup COLD butter
- 1 egg
- 1 cup milk

## Directions

1.  Preheat oven to 450 degrees F.

2.  The secret to excellent biscuits is COLD BUTTER. Really cold. Many times the biscuit dough gets worked so much that the butter softens before the biscuits even go in the oven. Try cutting the butter into small pieces and stick back in the fridge pulling out only when ready to incorporate into the dough.

3.  Combine the dry ingredients and use a pastry cutter to cut the butter in. Don't go too crazy here - you want to see small, pea-sized pieces of butter throughout the dough.

4.  Add in the milk and egg and mix just until the ingredients are combined. The dough will be sticky but don't keep working it. You should be able to see the butter pieces in the dough.

5.  Turn the dough out onto a generously floured surface. Sprinkle some flour on to the top of dough so it won't stick to your fingers and knead 10-15 times. If the dough is super sticky just sprinkle some additional flour.

6.  Roll or pat the dough out to ¾ - 1 inch thickness and cut with a biscuit cutter or glass. I ended up with nine this time but depending on who is snacking on biscuit dough, I can get up to 12 biscuits. Place the biscuits on a lightly greased baking sheet and bake for 10-15 minutes or until golden brown.

7.  For extra yumminess, brush the tops of the biscuits with melted butter...
