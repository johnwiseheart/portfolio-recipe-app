---
date: 2018-05-30 16:30:00 -0700
instagram_hashes:
  - BjbVzaNBRmH
---

# Fruit Tart

## Ingredients

**Crust**

- 1/2 cup confectioners' sugar
- 1 1/2 cups all-purpose flour
- 1 1/2 sticks unsalted butter, softened and sliced

**Filling**

- 1 (8-ounce) package cream cheese, softened
- 1/2 cup granulated sugar
- 1 teaspoon vanilla extract

**Topping**

- Fresh strawberries, kiwi slices, blueberries, raspberries

**Glaze**

- 1 (6-ounce) can frozen limeade concentrate, thawed
- 1 tablespoon cornstarch
- 1 tablespoon fresh lime juice
- 1/4 cup granulated sugar
- Whipped cream, for garnish

## Directions

**Crust**

1.  Preheat the oven to 350 degrees F.

2.  In a food processor, combine the confectioners' sugar, flour, and butter, and process until the mixture forms a ball.

3.  With your fingers, press the dough into a 12-inch tart pan with a removable bottom, taking care to push the crust into the indentations in the sides. Pat until the crust is even.

4.  Bake for 10 to 12 minutes, until very lightly browned. Set aside to cool.

**Filling and Topping**

1.  Beat the cream cheese, sugar, and vanilla together until smooth. Spread over the cooled crust.

2.  Cut the strawberries into 1/4-inch slices and arrange around the edge of the crust.

3.  For the next circle, use kiwi slices.

4.  Add another circle of strawberries, filling in any spaces with blueberries.

5.  Cluster the raspberries in the center of the tart.

**Glaze**

1.  Combine the limeade, cornstarch, lime juice, and sugar in a small saucepan and cook over medium heat until clear and thick, about 2 minutes. Let cool.

2.  With a pastry brush, glaze the entire tart. You will not use all of the glaze.

## Notes

- Keep the tart in the refrigerator. Remove about 15 minutes before serving. Slice into 8 wedges and serve with a dollop of whipped cream.
