---
date: 2018-04-22 16:30:00 -0700
---

# Raspberry White Chocolate Ganache

## Ingredients

- 1/4 cup heavy cream
- 1 cup good quality white chocolate chips
- 1 tsp Raspberry extract
- Red gel food coloring

## Directions

1.  Heat heavy cream in a small saucepan over medium heat until the cream becomes hot, but not boiling.

2.  Place the white chocolate chips in a bowl and pour the hot cream over them. Let it sit for a minute making sure the chips are completely submerged under the cream.

3.  Stir the chocolate and cream with a whisk

4.  Add the raspberry extract and food coloring. Mix until it becomes a smooth consistency.

## Notes

- Make sure cream is hot enough when poured over white chocolate
