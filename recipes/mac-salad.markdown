---
date: 2018-05-11 16:30:00 -0700
---

# Hawaiian Mac Salad

## Ingredients

- Salt and freshly ground black pepper
- 1 pound elbow macaroni
- 1/2 cup cider vinegar
- 2 cups whole milk
- 2 cups mayonnaise
- 1 tablespoon brown sugar
- 4 scallions, thinly sliced
- 2 carrots, peeled and grated
- 2 stalks celery, finely chopped

## Directions

1.  In a large stockpot, bring 4 quarts water and 1 tablespoon salt to a boil, Stir in pasta and cook until very soft, about 15 minutes, stirring often.

2.  Drain well, transfer to a large bowl, and stir in vinegar. Cool at least 10 minutes.

3.  Meanwhile, in a small bowl, whisk together milk, mayonnaise, brown sugar, 1/2 teaspoon salt, and 2 teaspoons pepper. Pour over cooled noodles and stir until evenly coated.

4.  Fold in scallions, carrots, and celery and stir to combine. Season to taste with salt and pepper. Cover and refrigerate at least 1 hour.

## Notes

- Cook your macaroni to the point of overcooking it. The noodles should be soft, fat, and ready to soak up lots of sauce.
- Try adding pineapple and coconut?
