---
date: 2018-12-29 16:30:00 0000
instagram_hashes:
  - Br_SUqdhHqP
---

# Milkybar-chunk cookies

## Ingredients

- 150g (10 tbsp) soft butter
- 100g (1/2 cup) soft light brown sugar
- 100g (1/2 cup) caster sugar
- 2 tsp vanilla extract
- 1 egg
- 1 egg yolk
- 280g (2 1/4 cups) plain flour
- 1 tsp bicarbonate of soda (baking soda)
- Pinch of salt
- 250g (9 oz) Milky Bar buttons or chunks of Milky Bar (or your favourite white chocolate)

## Directions

1. Line 2-3 baking trays with greaseproof paper.

2. In a large bowl, beat together your butter, sugars and vanilla extract until smooth and combined. It doesn't quite need to reach the fluffy stage!

3. Add you eggs one at a time, beating well between each one to thoroughly mix them in.

4. Add your flour, bicarbonate of soda and salt and stir well to combine.

5. Add your chunks or buttons of Milky Bar white chocolate and stir through.

6. Scoop dollops of the thick cookie dough onto your prepared trays, leaving AT LEAST 7cm (over 3 inches) between them to allow for spreading in the oven.

7. You should get anywhere between 16-24 lumps of cookie dough from this mixture, depending on how large you would like your cookies to be.

8. Once scooped, use your hands to roll the cookie dough lumps into round balls and place them back on the baking tray. This helps keep a nice even shape when baked.

9. Pop your trays of cookie dough into the fridge to chill for at least 1 hour, or as long as you can, really. You can chill overnight for good results

10. While your dough chills, preheat our oven - 190°C / 170°C fan / 375°F / gas mark 5.

11. Bake the cookies one tray at a time for 14-16 minutes, at this stage they should be a fairly even golden colour. They may look/feel underbaked but this is normal! Smaller cookies will need less time than larger cookies. If you prefer softer, chewier cookies, then less time is better too!

12. Allow the cookies to cool completely on a wire rack, atleast until the inside finishes setting.

## Notes

- Based on recipe from [Maverick Baking](http://maverickbaking.com/milky-bar-cookies/)
- It's important to take the cookies out before they feel done. Its the right time as they start to darken. They may look/feel underbaked but this is normal.
