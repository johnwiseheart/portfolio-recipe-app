---
date: 2019-01-04 16:30:00 -0700
instagram_hashes:
  - BsO3bnHB-C1
---

# Lemon Bars

## Ingredients

**Crust**

- 1 and 3/4 cups (220 grams) all-purpose flour (spooned & leveled)
- 1/4 cup (32 grams) cornstarch
- 1/2 cup (100 grams) granulated sugar
- 1/4 teaspoon salt
- 1 cup (230 grams) unsalted butter, softened

**Lemon filling**

- 1 and 1/2 cups (300 grams) granulated sugar
- 1/4 cup (32 grams) all-purpose flour
- 4 large eggs
- 1/2 cup (120ml) fresh lemon juice (3-4 lemons)

## Directions

**Crust**

1. Preheat oven to 175 degrees C.

2. Line a 9x13 baking pan with aluminum foil or parchment paper, making sure to leave some overhang for easy removal. Set aside.

3. In a large mixing bowl, whisk together the flour, cornstarch, sugar, and salt.

4. Cube the butter into smaller pieces and add to the flour mixture. Using a pastry cutter (or your hands) cut the butter into the mixture until it's crumbly and starts to come together. It doesn't matter if theres still some bigish bits of butter.

5. Scoop the mixture into the prepared baking pan and press it down into one even layer.

6. Bake at 175 degrees C for 20-25 minutes, or just until the top is set and edges are lightly golden brown. Remove from the oven and set aside. Keep oven temperature at 175.

**Lemon filling**

1. In a large mixing bowl, whisk together the granulated sugar and flour.

2. Add in the eggs and lemon juice and mix until fully combined. Pour the lemon filling over the crust and return to the oven.

3. Bake at 175 C for 18-22 minutes or until the lemon filling is set.

4. Remove from the oven and transfer to a wire rack to cool for 1 hour, then cover tightly, and refrigerate for at least two hours.

5. Sift powdered sugar on top and slice into bars.

## Notes

- Original recipe from [Live Well Bake Often](https://www.livewellbakeoften.com/classic-lemon-bars/).
- Lemon bars may be stored in an airtight container in the refrigerator for up to four days.
