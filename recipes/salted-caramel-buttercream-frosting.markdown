---
date: 2018-06-10 16:30:46 -0700
---

# Salted Caramel Buttercream Frosting

## Ingredients

**Salted Caramel**

- 3/4 cup granulated sugar
- 2 Tablespoons water
- 1/2 cup heavy cream, room temperature
- 1/2 Land O Lakes® Unsalted Butter Stick (1/4 cup), cubed, room temperature
- 1/4 teaspoon pure vanilla extract
- 2 teaspoons sea salt

**Salted Caramel Buttercream Icing**

- 1 Unsalted Butter Stick (1/2 cup), softened
- 1 Salted Butter Stick (1/2 cup), softened
- 1 Tablespoon pure vanilla extract
- 6 cups powdered sugar
- 1/2 cup salted caramel, cooled

## Directions

**Salted Caramel**

1.  Add 3/4 cup sugar and 2 tablespoons water to a (preferably stainless steel) pot (that has a heavy bottom and high sides). Mix to combine and then turn heat to medium high.

2.  Lift the pot and swirl but don’t stir. Cook until the sugar turns to a caramel color, about 10 to 11 minutes (watch carefully so it doesn’t burn). It needs to bubble to cook, but it turns super quick so pay lots of attention.

3.  Turn heat down to low. Add butter (it will be really hot so be careful) and whisk until thoroughly combined. Again, being careful, add heavy cream and whisk thoroughly.

4.  Remove from heat and add vanilla extract and salt (add more if you like it even more salty) and whisk thoroughly. Allow to cool completely before putting into an airtight container and refrigerating.

**Salted Caramel Buttercream**

1.  In your stand mixer bowl using the paddle attachment, mix thoroughly the softened 1 Land O Lakes® Unsalted Butter Stick and 1 Land O Lakes® Salted Butter Stick on low to medium speed. Add vanilla extract and mix well.

2.  Turn off your mixer and in two batches, add the powdered sugar and mixing until well blended (it will look like a dough). Be careful, the powdered sugar can go everywhere!

3.  Add salted caramel sauce and mix for about 3 to 4 minutes. I like to refrigerate my buttercream before using.

4.  Refrigerate it for at least 30 minutes (overnight even), just place on the kitchen counter for a little while if icing cupcakes. For macarons, start as fast as possible because you take a long time, and it melts quickly.

## Notes

- From [The Little Kitchen](https://www.thelittlekitchen.net/double-chocolate-cupcakes-with-salted-caramel-buttercream)
- This makes a shitton of icing - probably a good amount for cupcakes but way too much for macarons
