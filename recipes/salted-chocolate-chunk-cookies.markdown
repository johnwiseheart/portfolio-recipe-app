---
date: 2019-11-29 16:30:00 -0700
instagram_hashes:
  - B5d9gtCBBIi
---

# Salted Chocolate Chunk Cookies

## Ingredients

- 1 1/2 cups all-purpose flour
- 1 teaspoon baking powder
- 1/2 teaspoon kosher salt
- 1/4 teaspoon baking soda
- 1/2 cup (1 stick) unsalted butter, room temperature
- 3/4 cup (packed) light brown sugar
- 1/2 cup sugar
- 1/4 cup powdered sugar
- 2 large egg yolks
- 1 large egg
- 1 teaspoon vanilla extract
- 8 ounces semisweet or bittersweet chocolate (do not exceed 72% cacao), coarsely chopped
- Maldon or other flaky sea salt

## Directions

1. Place racks in upper and lower thirds of oven and preheat to 375°. Whisk flour, baking powder, kosher salt, and baking soda in a medium bowl; set aside.

2. Using an electric mixer on medium speed, beat butter, brown sugar, sugar, and powdered sugar until light and fluffy, 3-4 minutes. Add egg yolks, egg, and vanilla. Beat, occasionally scraping down the sides of the bowl, until mixture is pale and fluffy, 4-5 minutes. Reduce mixer speed to low; slowly add dry ingredients, mixing just to blend. Using a spatula, fold in chocolate.

3. Spoon rounded tablespoonfuls of cookie dough onto 2 parchment paper-lined baking sheets, spacing 1-inch apart. Sprinkle cookies with sea salt.

4. Bake cookies, rotating sheets halfway through, until just golden brown around the edges, 10-12 minutes (the cookies will firm up as they cool). Let cool slightly on baking sheets, then transfer to wire racks; let cool completely.

## Notes

- Recipe from [Bon Appetit](https://www.bonappetit.com/recipe/salty-chocolate-chunk-cookies).
