---
date: 2018-07-16 16:30:00 -0700
instagram_hashes:
  - BlTBGCDhyfR
---

# Mexican Wedding Cakes

## Ingredients

- 1 cup butter, softened
- 1/2 cup powdered sugar
- 1 teaspoon vanilla
- 2 1/4 cups sifted flour
- 1/4 teaspoon salt
- 3/4 cup chopped walnuts or 3/4 cup pecans
- Powdered sugar (for rolling baked cookies in)

## Directions

1.  Cream together butter and powdered sugar until light and fluffy; stir in vanilla.

2.  Whisk together flour and salt; add gradually to butter mixture; stir in chopped nuts.

3.  Chill dough if it seems too soft.
4.  Form dough into 1 1/4" balls and place onto parchment-lined or ungreased baking sheets.

5.  Bake at 400° for 10-12 minutes or just until the cookies start to turn light golden-brown.

6.  Remove from oven and allow to cool slightly; while cookies are still warm (but NOT hot) remove them from baking sheets and roll, a few at a time, in powdered sugar until evenly coated; cool cookies completely on wire racks.

7.  Cookies may (optionally) be rolled in powdered sugar a second time once cooled to room temperature.

## Notes

- From [Genius Kitchen](http://www.geniuskitchen.com/recipe/traditional-mexican-wedding-cookies-162213)
- Forming dough into 1" balls will increase yield to 48 cookies.
