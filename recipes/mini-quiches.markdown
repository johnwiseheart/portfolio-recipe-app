---
date: 2018-12-25 16:30:00 0000
instagram_hashes:
  - Br5WLy2BjL4
---

# Mini Quiches

## Ingredients

- 2 rectangular sheets puff pastry (or 4-5 square sheets)
- 3-4 large eggs
- 1 tablespoon stock/powder

Fillings:

- 1 packet of ham
- 1 bag of cheese (mozzarella/cheddar blend does well, but tasty works too)
- Chopped cherry tomatoes

## Directions

1. Grease 2 mini-muffin pans (nonstick pans don't need as much greasing).

2. Chop ham and tomato into small pieces, I usually quarter cherry tomatos, and chop ham into 1cm squares.

3. Beat eggs with stock.

4. Roll out sheets of pastry, and cut rounds using a cookie cutter. You can also use a glass or mug. Fill the muffin pan with your rounds, pressing lightly to make sure they fill the pan.

5. Add a small amount of each filling - typically ham, then cheese, then tomato. Make sure not to fill too far past the edge.

6. Top each quiche up with a spoon or two of egg mixture, again making sure not to go too close to the edge.

7. Clean up any mess you have made on the pan, and chuck them into the oven at 180 degrees C.

8. After 10-12 minutes take a look and see how they're going - once they're puffy and golden then its time to take them out.

9. Using a knife, load them onto a cooling rack and let them cool for a hot second, before serving them warm.

## Notes

This recipe is super simple and malleable - you can swap out any of the fillings and it will still come out great. I'd imagine that spinach, or bacon would work well too.
