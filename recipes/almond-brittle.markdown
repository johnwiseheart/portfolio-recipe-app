---
date: 2019-05-05 16:30:00 -0000
instagram_hashes:
  - BxFcT5Qhv7Q
---

# Almond brittle

## Ingredients

- 200g toasted flaked almonds
- 200g golden caster sugar
- 100g butter
- 1 tsp salt
- 8 tbsp water

## Directions

1. Lightly butter a lipped baking tray and spread the almonds over it in a thin, even layer.

2. Put the sugar, butter, salt and water into a smooth-bottomed frying pan and heat gently, stirring, until the sugar dissolves. Increase the heat and boil for about 5 mins, or until it starts to turn light golden brown. Stir every now and again as some patches will start to caramelise faster than others. Carefully tip the caramel over the nuts and leave to cool completely. If you poured unevenly, use a silicon spatula to even out the caramel.

3. Once cold, shatter the caramel. An easy way to get clean sharp pieces is to put the point of a sharp knife vertically onto the caramel and bash the handle with a rolling pin, like a hammer and chisel. Serve on top of the Chocolate amaretti puddings.

## Notes

- Originally from [BBC Good Food](https://www.bbcgoodfood.com/recipes/3203/salted-almond-brittle). If you prefer a more sugary, caramel-y brittle, you can use less almonds. If you prefer more almond-y, less sugar/butter can be used, but make sure that you cook enough caramel to cover them all lightly.

- Can keep for up to 2 days in an airtight container, interleaved with non-stick paper, in a cool, dry place (not the fridge).

- If you don't have toasted flaked almonds, you can buy regular flaked almonds and toast them yourself. In a non-stick fry pan, add a small amount of the flaked almonds at a time, and on medium-high heat let them toast up. Stir regularly, and take them out once theyre starting to go golden. You will probably need to repeat this 5-6 times to get through the 200g.
