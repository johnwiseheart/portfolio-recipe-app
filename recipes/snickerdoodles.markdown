---
date: 2018-06-23 16:30:00 -0700
instagram_hashes:
  - BkWRLBoBhRJ
---

# Snickerdoodles

## Ingredients

- 1/2 cup butter, softened
- 1/2 cup shortening
- 1 1/2 cups granulated sugar
- 2 eggs
- 2 teaspoons vanilla extract
- 2 3/4 cups all-purpose flour
- 2 teaspoons cream of tartar
- 1 teaspoon baking soda
- 1/4 teaspoon salt
- 4 Tablespoons granulated sugar (for rolling the dough balls in)
- 1 Tablespoons ground cinnamon (for rolling the dough balls in)

## Directions

1.  Preheat oven to 375 degrees F.

2.  Cream together butter, shortening, and 1 1/2 cups sugar. Add eggs and vanilla and mix well.

3.  Add the flour, cream of tartar, baking soda and salt and stir until combined.

4.  Shape dough by spoonfuls into balls. I grease my hands with cooking spray since the dough is slightly sticky. I also like to put one finished tray of rolled cookies in the fridge while I’m rolling the second. Then when you're ready to roll them in cinnamon and sugar they are easier to handle.

5.  In a separate bowl, mix the 4 tablespoons granulated sugar and the cinnamon. Roll balls of dough in mixture. Place dough about 2 inches apart on a parchment lined baking sheet.

6.  Bake 9 to 10 minutes (don't over bake!) Remove immediately (and carefully, with a good spatula!) from baking sheets onto a cooling rack to cool completely.

## Notes

- From [Tastes Better From Scratch](https://tastesbetterfromscratch.com/snickerdoodles/)
- Original recipe said to bake 8-9 minutes but I think its better to bake them a little longer, maybe 9-10 minutes. They can still be slightly undercooked coming out of the oven but by the time they’re cool, they taste good.
