---
date: 2019-11-28 16:30:00 -0700
instagram_hashes:
  - B0_FJw5hBbU
---

# Roasted Garlic Rosemary Focaccia, with Caramelized Onions

## Ingredients

- 1 1/3 cup warm water (105-110F to be exact)
- 2 teaspoons sugar
- 1 (0.25 ounce) packet instant yeast (2 1/4 teaspoons)
- 3 3/4 cup flour, plus a little more
- 2 teaspoons kosher salt
- 1/2 cup olive oil, plus more for greasing
- 1 tablespoon chopped rosemary
- 1 big garlic, or 2 little ones
- 1 lb onions
- 1⁄4 cup olive oil
- flaked sea salt (for topping)

## Directions

1. Chuck the oven on 400F. Peel as much away from your garlic as you can, while keeping it in one bit. Cover in olive oil, wrap it up in foil, and put it in the oven for about an hour. Pull it out, separate out the garlic from the skin,a and give it a bit of a chop.

2. Slice the onions finely. Pour the oil into a pan and fry the onions until soft and golden brown. If you want to be fancy, you could try adding a little salt and balsamic vinegar after about 10-15 minutes of frying.

3. Add the warm water, sugar, and yeast to a 2-cup measuring cup. Give it a stir and allow the yeast to bloom for 15 minutes. You’ll see the yeast is foamy and bubbly near the end of the 15 minutes.

4. Meanwhile, add the flour, salt, 1/4 cup of olive oil, chopped rosemary, and roasted garlic to the bowl of a stand mixer. Using the dough hook attachment, turn the mixer on briefly to allow the ingredients to mix together. Pour the yeast mixture into the flour mixture with the mixer running on low. Increase the speed to medium and let the mixer run for 4-5 minutes. Stop and scrape the dough if all the flour doesn’t incorporate fully. After 5 minutes, if the dough seems too sticky, use an additional 1-2 tablespoon of flour and form it into a ball using your hands. Instead of using a standmixer, you can also kneed by hand for 10-15 minutes.

5. Transfer the dough into a large bowl that has been greased. Rub a dime size of olive oil on the surface of the dough and cover with a piece of plastic wrap. Allow it to rest in a warm place for 1-1 1/2 hours or until it’s nearly doubled in size. If its cold, you can do this in a clothes dryer (run it on high temp for a couple of minutes first).

6. Position a rack in the center of the oven and preheat the oven to 400F. Using half of the remaining olive oil (2 tbsp), grease the bottom of a 9×13 dish (affiliate link). Remove the plastic wrap, and dump the dough into the prepared dish. Gently, using your fingers, push the dough out so that it fits the pan. Cover the dough with the plastic wrap and let sit for 20 minutes.

7. Brush the remaining 2 tablespoons of olive oil onto the dough. Using your fingers poke holes in the surface of the dough (seriously, poke all the way to the pan!). Cover with your onions, and a little flaked sea salt.

8. Bake the bread for 20-25 minutes or until golden brown on top and cooked all the way through. Remove from the oven, drizzle or brush with a little bit more olive oil and let cool for a few minutes before slicing and serving!

## Notes

- This recipe is a combination of this [this](https://littlespicejar.com/roasted-garlic-rosemary-focaccia-bread/) recipe from LittleSpiceJar, as well as my own additions.

- You can try other toppings, sauteed mushrooms work well, as does cheese & bacon.
