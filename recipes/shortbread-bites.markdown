---
date: 2018-06-10 16:30:46 -0700
instagram_hashes:
  - BjLHGOFhx4Q
---

# Shortbread bites

## Ingredients

- 1 cup (226g) salted butter, cold and diced into 1 Tbsp pieces
- Whisk in 1/2 tsp salt with the flour before adding if using unsalted butter
- Consider cutting up butter, then putting it back in the fridge so its super chilled
- 2/3 cup (142g) granulated sugar
- 1/2 tsp vanilla (or almond) extract
- 2 1/2 cups (318g) all-purpose flour (scoop and level flour to measure)
- 2 1/2 Tbsp (30g) nonpareils sprinkles (any color), then about 1/2 Tbsp more for tops if desired

## Directions

1.  Butter a 13 by 9-inch baking dish and line with 2 sheets of parchment paper (one horizontally and one vertically. I also like to butter between parchment sheets so they stick) leaving a 1-inch overhang on all sides, set aside.

2.  In the bowl of an electric stand mixer cream together butter and sugar until well combined.

3.  Mix in vanilla extract.

4.  With mixer set on low speed slowly add in flour and mix until combined (it will take a while and will appear dry and sandy at first but it will start to come together).

5.  Mix and fold in sprinkles.

6.  Press dough into an even layer in prepared pan (I did it with my hands first then used the bottom of a flat measuring cup to help smooth it out). Chill dough in refrigerator 20 - 30 minutes. Meanwhile preheat oven to 350 degrees F.

7.  Lift dough from pan using parchment overhang on all sides. Cut into 1/2-inch squares using a large sharp knife (it works best to just cut long rows).

8.  Sprinkle more sprinkles over the top if desired and gently press into dough.

9.  Transfer about 1/3 of the cookies to an unbuttered baking sheet (also do not line pan with parchment or silicone liners) and scatter cookies out spacing cookies 1/2-inch apart.

10. Bake in preheated oven 8 - 12 minutes (keep remaining that aren't currently baking refrigerated). Repeat process with remaining bites, adding them to a cool baking sheet.

11. Cool completely then transfer to an airtight container and store at room temperature.

## Notes

- From [Cooking Classy](https://www.cookingclassy.com/funfetti-shortbread-bites/)
- Make sure that you cream the butter and sugar together properly - use the dough attachment to a mixer - if its super crumbly when you put it into the tray its not going to come out well.
