---
date: 2018-04-22 16:30:00 -0700
instagram_hashes:
  - Bjp_d_aBuI6
  - Bh5MduUB47g
---

# Macarons

## Ingredients

**Macaron**

- 4 egg whites (room temperature) (138g)
- 1/3 cup white sugar (70g)
- 2 1/2 cups Powdered sugar (sifted) (275g)
- 1 1/2 cups almond flour (sifted) (151g)
- Pinch of salt (1tsp)
- 1/2 tsp cream of tartar
- Gel food coloring of relevant color

**Filling**

- See [Raspberry White Chocolate Ganache](/recipes/raspberry-white-chocolate-ganache/)
- See [Salted Caramel Buttercream](/recipes/salted-caramel-buttercream/)

## Directions

1.  Preheat oven to 300F (148C) degrees. Make sure fan forced is off.

2.  Sift the almond flour and powdered sugar into a bowl and mix to combine. Sifting is crucial because you want to get rid of any larger pieces of almond.

3.  Beat the egg whites on high speed until foamy, then add the salt (pinch) and cream of tartar (½ tsp). Mix for another minute then add the sugar (⅓ cup). Beat for another 4-5 minutes or until stiff peaks form.

4.  Fold flour/sugar mixture into the egg white mixture. Be careful NOT to over mix. About 80 turns of your spatula should be enough. You want the batter to resemble the consistency of molten lava. I recommend using a large spatula. That way every turn will mix more of the flour into the egg whites.

5.  Fill a pastry bag with the batter using the glass technique. Twist the tip of the piping bag and clamp the end with your hand. This will make it easier to fill the bag. Using a circle piping tip about 1/2 inch in diameter, pipe out 1 inch mounds. Make sure to pipe out onto parchment paper. Do not add any grease to the cookie sheets. When I pipe out the batter, I don’t swirl the tip. I just keep it in one place until there’s enough piped out for 1 cookie.

6.  Take the cookie sheets and bang them onto the counter a few times to get rid of any air. I hit them pretty hard, about 10 times.

7.  Sprinkle cinnamon on to uncooked macarons if making salted caramel macarons.

8.  Let the macarons sit in room temperature for about 20-50 minutes or until they become tacky to the touch. If you live in an area with a lot of humidity, it may take longer for the macarons to form the “skin”.

9.  Bake 1 tray at a time for 18 minutes. Be careful NOT to under bake or else they will stick to the parchment paper. I found that the second batch that sat out while the first was baking tended to burn a little since it sat out longer. So you may want to remove the second batch from the oven a minute or two earlier.

10. Fill a disposable pastry bag with the filling (using the glass trick) and snip the tip of the bag using scissors. For each cookie, put filling on half, and then put the other half on. Stick them in an airtight container in the fridge for up to 7 days.

## Notes

- I left the macarons out too long before putting them in the oven leading to the skin forming too quickly and the top of the shell being very fragile. Monitor the tackiness more closely.

- Size was good, but need to make sure the shells are far enough apart

- Put a good amount of ganache in each one

- When piping, just do one big pipe in the middle and then push down to get it to end - this makes sure they’re even
