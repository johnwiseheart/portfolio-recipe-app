---
date: 2019-04-21 16:30:00 -0000
instagram_hashes:
  - BwiGADaBEtY
---

# ANZAC biscuits

## Ingredients

- 1 cup plain flour
- 1 cup rolled oats
- 1 cup brown sugar
- 1/2 cup coconut
- 125 g butter
- 2 tbsp golden syrup
- 1 tbsp water
- 1/2 tsp bicarbonate of soda

## Directions

1. Sift the flour into a bowl. Add the sugar, rolled oats and coconut.

2. Melt the butter in a saucepan, then add golden syrup and water.

3. Stir the bicarbonate of soda into the liquid mixture.

4. Add the liquid to the dry ingredients and mix thoroughly.

5. Place walnut-sized balls of mixture on a greased tray and bake at 175C for 15-20 minutes.

6. Biscuits will harden when cool.

## Notes

- From [Best Recipes](https://www.bestrecipes.com.au/recipes/quick-easy-anzac-biscuits-recipe/jloz2cs4).
