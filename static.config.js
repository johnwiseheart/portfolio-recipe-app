import path from 'path'
import fs from 'fs'
import grayMatter from 'gray-matter'

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

export default {
  plugins: [
    'react-static-plugin-typescript',
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages'),
      },
    ],
    require.resolve('react-static-plugin-reach-router'),
  ],
  entry: 'index.tsx',
  getSiteData: () => ({
    title: 'John Wiseheart',
  }),
  getRoutes: async () => {
    const markdown = []

    const normalizedPath = path.join(__dirname, 'recipes')
    fs.readdirSync(normalizedPath).forEach(async fileName => {
      const file = fs.readFileSync(normalizedPath + '/' + fileName, 'utf-8')
      const slug = fileName.split('.')[0]
      const name = toTitleCase(slug.replace(/-/g, ' '))
      markdown.push({ file, slug, name })
    })

    const recipes = markdown.map(md => ({
      name: md.name,
      slug: md.slug,
      ...grayMatter(md.file),
    }))

    return [
      {
        path: '/recipes',
        getData: () => ({ recipes }),
        children: recipes.map(recipe => ({
          path: `/${recipe.slug}`,
          template: 'src/containers/Recipe',
          getData: () => ({
            recipe,
          }),
        })),
      },
    ]
  },
}
